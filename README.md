# Docker repo

**Current totals**
* QC questions: 
* Quiz questions: 
* Total examples: 
* Standardized examples: 

## Prerequisites & Learning Objectives
Prerequisites:
* Linux command-line basics

After completing all the modules in this repository, associates should be able to:
* Explain the features and benefits of containerization tools like Docker
* Describe the differences between virtual machines and containers
* Containerize an existing application
* Use docker commands to build images, spin up containers, and view container statuses
* Attach to containers, detach, and view container logs
* Push and pull images from container repositories like DockerHub

## Contributing
For more information, please refer to the [meta repo](https://gitlab.com/revature_training/training).

## Module Definitions
Modules are listed below in recommended order to cover:
* [Docker concepts](./modules/docker-concepts)
* [Docker workflow](./modules/docker-workflow)
