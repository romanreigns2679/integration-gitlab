Install docker on a linux ec2
sudo yum install docker -y
*Note: -y means yes to install docker automatically*

Start Docker:
sudo service docker start

Create a user (no output):
sudo usermod -a -G docker ec2-user
*Note restart session afterward*

Get docker info:
docker info
*Note will fail until you restart your session*
