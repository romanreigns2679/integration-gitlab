Last Updated - ~Feburary 2018

1. Introduction to NPM
+ package.json
+ Introduction to Webpack/CLI
+ Bundling
+ Introduction to TypeScript
+ Access modifiers/variable types
+ Classes & Decorators
+ Introduction to Angular 4
+ Directives
+ Components
+ Modules and Templates
+ 2-way data binding
+ Pipes & Filtering
+ Angular Services & routing
+ Angular HTTP 
+ RxJS promises vs Observable
+ AngularJS vs Angular 4
+ Design Patterns: Publisher/Subscriber, Modules"