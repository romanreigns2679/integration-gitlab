# **Docker**

This demo will show how to host a spring boot application on a Docker container. This will be a walkthrough on how to create a Docker image using a Docker file and how to run a container from a Docker image. A Demo project, a JAR file of the project and a Dockerfile will be provided as part of the example. The JAR file and the Dockerfile will be the only necessary files for this demo (Build Project step can be skipped if necessary).

## **Prerequisites**

The following tools are used for this demo:

- [Git](https://gitforwindows.org/) 
- Java 8
- [Maven](https://maven.apache.org/)
- [Docker](https://docs.docker.com/docker-for-windows/install/)
- [Spring Tool Suite](https://spring.io/tools)


## **Installation**

To build and manage Docker containers, we will be using Docker Desktop. To run linux containers, WSL linux kernel package will also need to be installed. The specific instructions for a Windows home system can be found [here](https://docs.docker.com/docker-for-windows/install-windows-home/)

### **Build Project**

In this demo we will be utilising [Spring boot](https://start.spring.io/) to generate a simple web service that will return a list of planets. 

Next, we want to generate a simple JAR file from the project. To do so, we right click and select Run as -> Maven build. A new window will open (Edit configuration), inside of which there is Goals. Enter "clean compile install" into Goals and select Run. 

The JAR file will be created under the target folder of your project (by default a new project JAR file will look like "<name_of_project>-0.0.1-SNAPSHOT.jar" ). 

A JAR file has already been provided in the folder. Sending a GET request to http://localhost:9999/ will provide a list of planets. 

## **Dockerfile**

Inside the project, create a file "DockerFile" in the root folder.
Inside the file, insert:

    FROM openjdk:8-jdk-alpine //FROM layer defines what our image will be based on (the parent image to our child image)
    COPY target/demo-0.0.1-SNAPSHOT.jar demo.jar //COPY layer will copy the local jar file (built by Maven), in this case our jar file will be named demo.jar within our container
    CMD ["java","-jar", "/demo.jar"]` //CMD sets the start up command, in this case "java -jar /demo.jar"

- `FROM` is typically the first instruction and sets the base image. Images can be built on top of other pre-made images, images can be found on dockerhub. 
- `COPY` will copy images from the host machine to the container's file system. In this case the jar file is added to the image as "demo.jar"
- `CMD` will specify the runtime arguments to be executed.

This is one example of how a Docker File can be configured. Additional instructions exist for creating more advance [docker images](https://kapeli.com/cheat_sheets/Dockerfile.docset/Contents/Resources/Documents/index)

## **Generating a docker image and running a container**

Windows command line or Linux terminal (via Git Bash) can be used. This example will use Git Bash. 

1. Navigate to the project folder from the command line. `cd <filespace>`
2. Ensure docker is installed by checking what version it is: `docker  -v`
    - If Docker is installed, the result should be of a similar format to : `Docker version 19.03.8, build afacb8b` 
3. Create a docker image from the file using (-t allows the image to be named): `docker build -t <imagename> .` i.e. `docker build -t planetdemo .`
    - If `"docker build" requires exactly 1 argument"` appears, remember to put a full stop after the command.
    - `-t` allows us to associate a tag with the image.
    - To get a list of all the images available, input `docker image ls` 
4. To run the docker image: `docker run -d -p <host port>:<docker port>` i.e. `docker run -d -p 9999:9999`
    -  `-d` run the container in detached mode, container will run in the background 
    -  `-p` allows to map container ports to the host ports. The ports do not have to be the same.
5. To verify how many containers are currently running in your system: `docker ps` or `docker ps -a` for a list of all containers 
6. To shut down a container, input `docker container stop <container name or container id>`, to start a container input a similar command `docker container start <container name or container id>`




